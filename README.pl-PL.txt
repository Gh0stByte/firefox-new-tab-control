New Tab Control
===============

Ten dodatek do Firefoxa pozwala kontrolować wiele właściwości nowo otwartych kart.

Opcje (zobacz preferencje dodatku):

1. Linki otwarte w nowych kartach są umieszczane zaraz za aktywną kartą. Ustawienie 'browser.tabs.insertRelatedAfterCurrent' zostanie zignorowane.

2. Linki  otwarte z tej samej karty i zakładki otwarte jednocześnie są umieszczane za ostatnią kartą ze swojej grupy. Odtwarza to także zachowanie 'browser.tabs.insertRelatedAfterCurrent'.

3. Nowe puste karty (Ctrl+T) otwierane są zaraz za aktywną kartą. Użyj skrótu Ctrl+Alt+T aby otworzyć kartę w domyślnej pozycji.

4. Nowe puste karty zostaną umieszczone w domyślnej pozycji jeśli będą otwarte za pomocą myszy przez kliknięcie przycisku Nowej Karty lub podwójne kliknięcia paska kart.

5. Ładuje twoją stronę domową lub inny podany adres do nowo otwartej pustej karty.

6. Podaj własny adres URL, który zostanie użyty w nowych kartach. Pozostaw to pole puste, aby użyć strony domowej.

7. Zachowaj zmienioną zawartość paska adresu podczas otwierania nowej karty.

8. Uaktywnij stronę załadowaną do nowo otwartej karty zamiast jej paska adresu. Domyślnie wyłączone.

9. Nowe karty będą podświetlane losowym kolorem tła do czasu ich aktywacji. Dla zgrupowanych kart zostanie użyty ten sam kolor.

10. Podaj własne oddzielone przecinkami barwy (hue: 0-360, 361 dla białego, 362 dla czarnego) i przedziały barw, które zostaną użyte przy podświetlaniu. Pozostaw to pole puste, aby użyć wszystkich kolorów.

11. Obrót kółka myszy nad paskiem kart przełącza karty w przód i w tył. Użyj klawisza Shift by przeskoczyć od razu do pierwszej lub ostatniej karty, lub klawisza Ctrl by przełączyć na ostatnio aktywną kartę.

12. Zamknięcie aktywnej karty przełącza na kartę z lewej strony.

Strona domowa dodatku: https://addons.mozilla.org/firefox/addon/new-tab-control/


Kod źródłowy
------------

New Tab Control jest udostępniany na licencji GPL w wersji 3.

Kod źródłowy możesz znaleźć w serwisie GitLab:
https://gitlab.com/ultr/firefox-new-tab-control/

Tłumaczenia i łatki mile widziane.


Dotacje
-------

Jeśli podoba ci się mój dodatek, rozważ proszę wsparcie go drobną dotacją.
